package smtpdb.util

import scala.util.logging.Logged

import java.util.Date
import java.text.DateFormat

trait SMTPLogger extends Logged {

  override def log(msg: String) {
    print(format(msg))
  }
  
  def error(msg: String) {
    //Do some type of error-specific logging
    println("!! " + format(msg))
  }

  def format(msg: String) =  (new Date).toString() + " // " +  System.currentTimeMillis() + ") " + msg + "\n"
}
