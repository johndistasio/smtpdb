package smtpdb.core

import smtpdb.util.SMTPLogger

object SMTPMessage {
}

class SMTPMessage(toData: String, fromData: String, subjectData: String) extends SMTPLogger {

  val from: String = fromData
  val to: String = toData
  val subject: String = subjectData

  def outputMessageContents() = log("From: " + this.from + " To: " + this.to + " Subject: " + this.subject)

}
