package smtpdb.core

import smtpdb.util.SMTPLogger

import java.io._
import java.net._

object SimpleServer extends SMTPLogger {

  def main(args: Array[String]) {
    log("SimpleServer Started.")

    try { 
      val serverSocket = new ServerSocket(4444) 
    
      while(true) {
        val incommingConnection = new Connection
        incommingConnection.start()
        incommingConnection ! serverSocket.accept()
      }
      
      serverSocket.close()
    }
    catch {
      case e: IOException => error(e.getMessage())
    }


    

  }
}
