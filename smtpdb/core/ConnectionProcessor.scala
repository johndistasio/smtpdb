package smtpdb.core

import smtpdb.util.SMTPLogger

import scala.util.matching.Regex

import java.io.{BufferedReader, BufferedWriter, InputStreamReader, OutputStreamWriter}
import java.net.Socket

/**
 * I can't think of a better way to do this at the moment.
 */
object SMTPConnectionState extends Enumeration {
  val SENDING = Value("Sending")              //
  val RECEIVING = Value("Receiving")          //
  val DATA_PROCESS = Value("Processing Data") //we're processing the message body
  val ERROR = Value("Error")                  //
  val COMPLETE = Value("Complete")            //
}

object ConnectionProcessor {
  
  def email(str: String) = {
    val adr = str.split("@")
    if(adr.length == 2) (adr(0):String, adr(1):String) else false: Boolean
  }
  
  /* SMTP command regex extractors */
  /* Notes to self: (?i) = case insenstive search */
  
  /**
   * Matches HELO command.
   */
  val HELO = """(?i)^(helo.*)$""".r
  
  /**
   * Matches EHLO command.
   */
  val EHLO = """(?i)^(ehlo??\s.*)$""".r
  
  /**
   * Matches FROM command.
   */
  val FROM = """(?i)^(mail from:<.*>)$""".r
  
  /**
   * Matches RCPT command.
   */
  val RCPT = """(?i)^(rcpt to:<.*>)$""".r
  
  /**
   * Matches DATA command.
   */
  val DATA = """(?i)^(data$)""".r
  
  /**
   * Matches QUIT command.
   */
  val QUIT = """(?i)^(quit?\s*)$""".r

  //DATA-related extractors
  val DATA_CONTENT_TYPE = """(?i)^Content-Type:\s(.*);.*$""".r
  val DATA_MIME_VERSION = """(?i)^MIME-Version:\s(.*)$""".r
  val DATA_CONTENT_TRANSER_ENCODING = """(?i)^content-transfer-encoding:\s(.*)$""".r;
  val DATA_FROM = """(?i)^from:.*<(.*)>$""".r
  val DATA_TO = """(?i)^to:.*<(.*)>$""".r
  val DATA_CC = """(?i)^cc:.*<(.*)>$""".r
  val DATA_BCC = """(?i)^bcc:.*<(.*)>$""".r
  val DATA_DATE = """(?i)^date:(.*)$""".r
  val DATA_SUBJECT = """(?i)^subject:\s(.*)$""".r
  val DATA_BODY = """^(\w)*$""".r
  val DATA_END = """\r\n\.\r\n""".r
  val BLANK_LINE = """^\r\n""".r
}

class ConnectionProcessor(socket: Socket, actorName: String) extends SMTPLogger {
  import SMTPConnectionState._
  var state = SENDING

  val name = actorName
  val client = socket.getInetAddress.toString()
  val FQDN = socket.getLocalAddress().getCanonicalHostName()  
  val inputSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()))
  val outputSocket = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))



  def in():String = {
    val in = inputSocket.readLine()  
    log(name + "  in: " + in)
    in
  }
  
  def in2():String = {
    val in = inputSocket.readLine()
    in
  }

  def out(data: String) {
    log(name + " out: " + data)
    this.outputSocket.write(data + "\r\n")
    this.outputSocket.flush()
  }

  def greeting() = {
    out("220 " + FQDN + " ESMTP")
    this.state = RECEIVING
  }

  def ack() = out("250 Ok")
  
  //Need some sort of validation in these handle* methods.
  //As is, we could go right from EHLO to DATA. That's no good.
  //How best to keep track of the current state + valid progressions?
  //Is there a 'functional' way to do this?
 

  def handleHELO(input: String) = {
    val HELO = """(?i)^helo??\s(.*)$""".r
    val HELO(client) = input
    this.out("250 Ok Hello " + client)
  }

  def handleEHLO(input: String) = {
    val EHLO = """(?i)^ehlo??\s(.*)$""".r
    val EHLO(client) = input
    this.out("250 Ok Hello " + client)
  }
  
  def handleFROM(input: String) = {
    val FROM = """(?i)^mail from:??<(.*)>$""".r
    val FROM(sender) = input
    this.out("250 Ok email from " + sender)
  }
  
  def handleRCPT(input: String) = {
    val RCPT = """(?i)^rcpt to:??<(.*)>$""".r
    val RCPT(recipient) = input
    this.out("250 Ok email to " + recipient)
  }

  def handleDATA(input: String): Unit = {
    this.out("354 End data with <CRLF>.<CRLF>")
    this.state = DATA_PROCESS
    while(this.state == DATA_PROCESS) {
      this.processData(in2)
    }
    process(in)
  }
  
  def getSender(input: String) = {
    val DATA_TO_PARSER = """(?i)^to:??\s(.*)$""".r
    //DATA_TO_PARSER
  }


  def handleQUIT() = {
    this.out("221 Bye")
  }

  //Process data by with regex extractors
  //I think we need may need one each for the message headers.
  //then one for the body, then one for the end.
  def processData(input: String) = input match {
    case ConnectionProcessor.DATA_CONTENT_TRANSER_ENCODING(input) => log("DATA_CONTENT_TRANSFER_ENCODING Matched: " + input)
    case ConnectionProcessor.DATA_CONTENT_TYPE(input) => log("DATA_CONTENT_TYPE Matched: " + input)
    case ConnectionProcessor.DATA_MIME_VERSION(input) => log("DATA_MIME_VERSION Matched: " + input)
    case ConnectionProcessor.DATA_TO(input) => log("DATA_TO Matched: " + input)
    case ConnectionProcessor.DATA_FROM(input) => log("DATA_FROM Matched: " + input)
    case ConnectionProcessor.DATA_SUBJECT(input) => log("DATA_SUBJECT Matched: " + input)
    case ConnectionProcessor.DATA_END(input) => log("DATA_END matched."); this.state = RECEIVING
    case ConnectionProcessor.BLANK_LINE => log("BLANK_LINE matched.")
    case ConnectionProcessor.DATA_BODY(input) => log("DATA_BODY Matched: " + input)
    //case _ => println("Default: " + input)
  }


 
  def process(input: String) = input match {
    case ConnectionProcessor.HELO(input) => handleHELO(input)
    case ConnectionProcessor.EHLO(input) => handleEHLO(input)
    case ConnectionProcessor.FROM(input) => handleFROM(input)
    case ConnectionProcessor.RCPT(input) => handleRCPT(input)
    case ConnectionProcessor.DATA(input) => handleDATA(input)
    case ConnectionProcessor.QUIT(input) => handleQUIT()
  }

  def init() {
    log(name + " on " + socket.toString())
    greeting()
 
    process(in())
    process(in())
    process(in())
    process(in())

    socket.close()
      
  }
}
