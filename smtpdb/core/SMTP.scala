package smtpdb.core

/**
 * The SMTP object encapsulates the 'language' of the SMTP protocol. It
 * contains values that map to SMTP commands and let us validate 
 * incoming statements and parse useful data out of them.
 */
object SMTP {
  import scala.util.matching.Regex

  /* SMTP command regex extractors */
  /* Notes to self: (?i) = case insenstive search */
  
  /**
   * Matches HELO command.
   */
  val HELO = """(?i)^(helo.*)$""".r
  
  /**
   * Matches EHLO command.
   */
  val EHLO = """(?i)^(ehlo??\s.*)\r\n""".r
  
  /**
   * Matches FROM command.
   */
  val FROM = """(?i)^(mail from:<.*>)$""".r
  
  /**
   * Matches RCPT command.
   */
  val RCPT = """(?i)^(rcpt to:<.*>)$""".r
  
  /**
   * Matches DATA command.
   */
  val DATA = """(?i)^(data$)""".r
  
  /**
   * Matches QUIT command.
   */
  val QUIT = """(?i)^(quit?\s*)$""".r

  //DATA-related extractors
  val DATA_CONTENT_TYPE = """(?i)^Content-Type:\s(.*);.*$""".r
  val DATA_MIME_VERSION = """(?i)^MIME-Version:\s(.*)$""".r
  val DATA_CONTENT_TRANSER_ENCODING = """(?i)^content-transfer-encoding:\s(.*)$""".r;
  val DATA_FROM = """(?i)^from:.*<(.*)>$""".r
  val DATA_TO = """(?i)^to:.*<(.*)>$""".r
  val DATA_CC = """(?i)^cc:.*<(.*)>$""".r
  val DATA_BCC = """(?i)^bcc:.*<(.*)>$""".r
  val DATA_DATE = """(?i)^date:(.*)$""".r
  val DATA_SUBJECT = """(?i)^subject:\s(.*)$""".r
  val DATA_BODY = """^([aA-zZ]*[0-9]*)$""".r
  val DATA_END = """\r\n\.\r\n""".r
  val BLANK_LINE = """^\r\n""".r
}