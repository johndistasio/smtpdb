package smtpdb.core

import scala.actors._
import scala.collection.mutable.StringBuilder

import java.net.Socket

object Connection {
  
}

class Connection extends Actor {
  
  def act() {
    loop {
      react {
        case sock: Socket => new ConnectionProcessor(sock, this.toString()).init()
      }
    }
  }
}
